class Book < ActiveRecord::Base
  belongs_to :user
  has_many :meetings
  has_many :comments, dependent: :destroy
  
  acts_as_taggable # Alias for acts_as_taggable_on :tags

	def available?
		current_meeting.nil?
	end

	def current_meeting
		meetings.on_date.first
	end



end