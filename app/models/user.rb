class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :meetings 
  has_many :books


  	def current_book
  		current_meeting.book
  	end

	def current_meeting
		meetings.on_date.first
	end
end
