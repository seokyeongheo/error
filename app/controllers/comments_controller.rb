class CommentsController < ApplicationController
	before_action :set_book
	before_action :set_comment, only: :destroy

  def create
  	@comment = @book.comments.new(comment_params)
  	@comment.save
  end

  def destroy
  	@comment.destroy
  end

  private

  def set_book
  	@book = Book.find(params[:book_id])
  end

  def set_comment
  	@comment = @book.comments.find(params[:id])
  end

  def comment_params
  	params.require(:comment).permit(:body)
  end

end
